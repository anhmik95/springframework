DROP DATABASE IF EXISTS JPA;
CREATE DATABASE JPA;

use JPA;

drop table if exists Account;
create table Account (
                         id	        		int auto_increment primary key,
                         username			varchar(50),
                         email				varchar(50) not null,
                         password			varchar(50) not null
);

INSERT INTO JPA.`Account` (username, email, password) VALUES ('Nguyễn Văn A', 'a@gmail.com', '123456');
INSERT INTO JPA.`Account` (username, email, password) VALUES ('Nguyễn Văn B', 'b@gmail.com', '123456');
INSERT INTO JPA.`Account` (username, email, password) VALUES ('Nguyễn Văn C', 'c@gmail.com', '123456');