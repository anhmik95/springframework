package com.vti.springframework.controller;

import com.vti.springframework.modal.Account;
import com.vti.springframework.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/account")
public class AccountController {
    @Autowired
    private AccountService service;

    @GetMapping
//    @RequestMapping(method = RequestMethod.GET)   -- 2 cách để GET
    public List<Account> getAllAccount(){
        return service.getAllAccount();
    }
}
