package com.vti.springframework.service;

import com.vti.springframework.modal.Account;
import com.vti.springframework.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService implements IAccountService{

    @Autowired
    private AccountRepository repository;


    @Override
    public List<Account> getAllAccount() {
        return null;
    }

    @Override
    public Account getById(int id) {
        return null;
    }

    @Override
    public Account update(int id, Account account) {
        return null;
    }

    @Override
    public void create(Account account) {

    }

    @Override
    public void delete(int id) {

    }
}
